import {Component} from '@angular/core';
import {ApiClientService} from '../services/api-client.service';

@Component({
    selector: 'app-tab2',
    templateUrl: 'tab2.page.html',
    styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

    public facts: Array<Facts>;

    constructor(public api: ApiClientService) {
    }

    ionViewDidEnter() {
        this.api.getPopularFacts(20, 1).then(data => this.facts = JSON.parse(data.data));
    }
}
