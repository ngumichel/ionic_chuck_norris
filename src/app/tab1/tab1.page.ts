import {Component, OnInit} from '@angular/core';
import {ApiClientService} from '../services/api-client.service';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

    public facts: Array<Facts> = [];

    constructor(public api: ApiClientService) {
    }

    ionViewDidEnter() {
        this.api.getRandomFacts(60, 1).subscribe((data) => {
            // @ts-ignore
            this.facts = data;
        });
    }

}
